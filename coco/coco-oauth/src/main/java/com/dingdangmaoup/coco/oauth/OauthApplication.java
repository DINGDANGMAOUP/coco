package com.dingdangmaoup.coco.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;

/**
 * @author dzhao1
 */
@SpringBootApplication
public class OauthApplication {
  public static void main(String[] args) {
    //
    SpringApplication.run(OauthApplication.class,args);
  }
}

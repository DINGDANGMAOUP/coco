package com.dingdangmaoup.coco.monitor.util;

import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiRobotSendRequest;
import com.dingtalk.api.request.OapiRobotSendRequest.Link;
import com.dingtalk.api.request.OapiRobotSendRequest.Markdown;
import com.dingtalk.api.request.OapiRobotSendRequest.Text;
import com.dingtalk.api.response.OapiRobotSendResponse;
import com.taobao.api.ApiException;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

/** @author kuroneko */
@Component
public class DingTalkUtil {

  /** 参考 https://developers.dingtalk.com/document/app/custom-robot-access/title-zob-eyu-qse */
  @Resource DingTalkClient dingTalkClient;

  @Resource OapiRobotSendRequest request;
  @Resource Text text;
  @Resource Link link;
  @Resource Markdown markdown;

  @SneakyThrows
  public void pushText(String ct, String mobile) {

    request.setMsgtype("text");
    text.setContent(String.format("【监控】%s", ct));
    request.setText(text);
    OapiRobotSendRequest.At at = new OapiRobotSendRequest.At();
    at.setAtMobiles(List.of(mobile));
    at.setIsAtAll(true);
    request.setAt(at);
    dingTalkClient.execute(request);
  }

  @SneakyThrows
  public void pushLink(String msgUrl, String picUrl, String tittle, String text) {
    request.setMsgtype("link");
    link.setMessageUrl(msgUrl);
    link.setPicUrl(picUrl);
    link.setTitle(tittle);
    link.setText(String.format("【监控】%s", text));
    request.setLink(link);
    dingTalkClient.execute(request);
  }

  @SneakyThrows
  public void pushMarkdown(String tittle, String text)  {
    request.setMsgtype("markdown");
    markdown.setTitle(tittle);
    markdown.setText(String.format("【监控】%s", text));
    request.setMarkdown(markdown);
    dingTalkClient.execute(request);
  }
}
